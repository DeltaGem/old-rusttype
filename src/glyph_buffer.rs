use super::Glyph;
use index_list::Index;

/// southbound API for the GSUB implementation
pub trait SubstitutionBuffer {
	/// used for single substitution and ligature substitution.
	/// Replaces the glyph at `pos` with `new_glyph`, integrating the glyphs at the positions given by `additional_sources`.
	/// The Iterator shall start at a higher index than `pos` and be strictly monotonically increasing, but stay in the bounds of the buffer (`len()` method)
	fn replace<I: Iterator<Item=usize>>(&mut self, pos: usize, new_glyph: Index<Glyph>, additional_sources: I);
	fn expand<I: Iterator<Item=(Index<Glyph>, u16)>>(&mut self, pos: usize, new_glyphs: I);
	fn get(&self, pos: usize) -> Option<Index<Glyph>>;
	fn len(&self) -> usize;
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
enum CharacterFragment {All, First, Last}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
struct ManipulationLog {
	glyph_offset: usize,
	char_offset: usize,
	fragment: CharacterFragment
}

#[derive(PartialEq, Eq, Debug)]
pub struct BasicGlyphBuffer {
	glyphs: Vec<Index<Glyph>>,
	manip: Vec<ManipulationLog>
}

impl ::std::iter::FromIterator<Index<Glyph>> for BasicGlyphBuffer {
	fn from_iter<I: ::std::iter::IntoIterator<Item=Index<Glyph>>>(iter: I) -> Self {
		BasicGlyphBuffer {
			glyphs: iter.into_iter().collect(),
			manip: Vec::new()
		}
	}
}

fn block_swap<T>(buffer: &mut[T], mut pivot: usize) {
	assert!(pivot <= buffer.len());
	let len = {buffer.len()};
	let mut range = 0..len;
	loop {
		let mut buf = &mut buffer[range.clone()];
		let latter_len = range.end - range.start - pivot;
		if  latter_len > pivot {
			if pivot == 0 {return}
			for i in 0..pivot {
				buf.swap(i, i+pivot);
			}
			range = (range.start+pivot)..range.end;
		} else {
			if pivot == len {return}
			for i in 0..latter_len {
				buf.swap(pivot - latter_len + i, pivot+i);
			}
			range = range.start..(range.start + pivot);
			pivot -= latter_len;
		}
	}
}

#[test]
fn test_block_swap() {
	let mut buf = [0, 1, 2, 3, 4, 5, 6, 7];
	block_swap(&mut buf, 5);
	assert_eq!(buf, [5, 6, 7, 0, 1, 2, 3, 4]);
}

fn char_offset_for(manip: &[ManipulationLog], pos: usize) -> usize {
	match manip.binary_search_by_key(&pos, |m| m.glyph_offset) {
		Ok(mut i) | Err(mut i) => {
			// if there are no explicitly offset characters, extrapolate from zero
			if i == 0 {return pos}
			if i == manip.len() {i -= 1}
			let ref_gl = manip[i].glyph_offset;
			// go to the first component of a ligature
			while i > 0 && manip[i-1].glyph_offset == ref_gl {i -= 1}
			let reference = manip[i];
			// the implicit char offset of a glyph is extrapolated from the first component of the preceding explicit
			reference.char_offset + pos - ref_gl
		}
	}
}

impl SubstitutionBuffer for BasicGlyphBuffer {
	fn replace<I: Iterator<Item=usize>>(&mut self, pos: usize, new_glyph: Index<Glyph>, pos_iter: I) {
		fn find<F: FnMut(&ManipulationLog) -> bool>(manip: &[ManipulationLog], mut f: F) -> usize {
			for (i, m) in manip.iter().enumerate() {
				if f(m) {return i}
			}
			manip.len()
		}
		fn insert_log_if_needed(manip: &mut Vec<ManipulationLog>, pos: usize) -> usize {
			let mpos = find(&manip, |x| x.glyph_offset >= pos);
			mpos + match find(&manip[mpos..], |x| x.glyph_offset > pos) {
				0 => {
					let offset = char_offset_for(&manip, pos);
					manip.insert(mpos, ManipulationLog {glyph_offset: pos, char_offset: offset, fragment: CharacterFragment::All});
					1
				},
				mlen => mlen
			}
		}
		self.glyphs[pos] = new_glyph;
		let mut iter = pos_iter.peekable();
		match iter.peek() {
			None => return,	// save unnecessary log for single substitution
			Some(_) => {}
		}
		let mut gskip = 0;
		let mut mpos = insert_log_if_needed(&mut self.manip, pos);
		let mut idx = pos;
		'outer: for nextpos in iter {
			assert!(nextpos < self.glyphs.len() && nextpos > idx);
			let nextmpos = mpos + find(&self.manip[mpos..], |x| x.glyph_offset >= nextpos);
			assert!(nextmpos <= self.manip.len());
			let mend = insert_log_if_needed(&mut self.manip, nextpos);
			insert_log_if_needed(&mut self.manip, nextpos + 1);
			// adjust glyph offset for subsequent ManipulationLogs
			for m in &mut self.manip[mpos..nextmpos] {
				m.glyph_offset -= gskip;
			}
			// move the ManipulationLogs for the glyph-to-be-deleted to the replaced character
			block_swap(&mut self.manip[mpos..mend], nextmpos - mpos);
			for m in &mut self.manip[mpos..][..mend-nextmpos] {
				m.glyph_offset = pos;
			}
			mpos = mend;
			// remove glyph
			if gskip > 0 {
				while idx + gskip < nextpos {
					self.glyphs.swap(idx, idx + gskip);
					idx += 1;
				}
			} else {
				idx = nextpos;
			}
			gskip += 1;
		}
		for m in &mut self.manip[mpos..] {
			m.glyph_offset -= gskip;
		}
		while idx + gskip < self.glyphs.len() {
			self.glyphs.swap(idx, idx + gskip);
			idx += 1;
		}
		self.glyphs.truncate(idx);
	}
	fn expand<I: Iterator<Item=(Index<Glyph>, u16)>>(&mut self, pos: usize, new_glyphs: I) {unimplemented!()}
	fn get(&self, pos: usize) -> Option<Index<Glyph>> {self.glyphs.get(pos).cloned()}
	fn len(&self) -> usize {self.glyphs.len()}
}

#[test]
fn test_replace() {
	let mut buf = BasicGlyphBuffer {
		glyphs: (0..5).map(|x| Index::new(x)).collect(),
		manip: (0..10).map(|x| ManipulationLog {glyph_offset: x/2, char_offset: x, fragment: CharacterFragment::All}).collect()
	};
	buf.replace(1, Index::new(5), Some(3).into_iter());
	assert_eq!(buf.glyphs, [0, 5, 2, 4].iter().map(|&x| Index::new(x)).collect::<Vec<_>>());
	assert_eq!(buf.manip, vec![
		ManipulationLog {glyph_offset: 0, char_offset: 0, fragment: CharacterFragment::All},
		ManipulationLog {glyph_offset: 0, char_offset: 1, fragment: CharacterFragment::All},
		ManipulationLog {glyph_offset: 1, char_offset: 2, fragment: CharacterFragment::All},
		ManipulationLog {glyph_offset: 1, char_offset: 3, fragment: CharacterFragment::All},
		ManipulationLog {glyph_offset: 1, char_offset: 6, fragment: CharacterFragment::All},
		ManipulationLog {glyph_offset: 1, char_offset: 7, fragment: CharacterFragment::All},
		ManipulationLog {glyph_offset: 2, char_offset: 4, fragment: CharacterFragment::All},
		ManipulationLog {glyph_offset: 2, char_offset: 5, fragment: CharacterFragment::All},
		ManipulationLog {glyph_offset: 3, char_offset: 8, fragment: CharacterFragment::All},
		ManipulationLog {glyph_offset: 3, char_offset: 9, fragment: CharacterFragment::All}
	]);
	buf = BasicGlyphBuffer {
		glyphs: (0..5).map(|x| Index::new(x)).collect(),
		manip: Vec::new()
	};
	buf.replace(1, Index::new(5), Some(3).into_iter());
	assert_eq!(buf.glyphs, [0, 5, 2, 4].iter().map(|&x| Index::new(x)).collect::<Vec<_>>());
	assert_eq!(buf.manip, vec![
		ManipulationLog {glyph_offset: 1, char_offset: 1, fragment: CharacterFragment::All},
		ManipulationLog {glyph_offset: 1, char_offset: 3, fragment: CharacterFragment::All},
		ManipulationLog {glyph_offset: 3, char_offset: 4, fragment: CharacterFragment::All}
	]);
} 
