mod parsing;
pub use parsing::*;

mod shaping;
pub use shaping::*;


use std::collections::VecDeque;
use self::SimpleSubstitution::{Single, Alternate, Multiple};
use self::GlyphIndexCalculator::{Offset, List};
use self::GSubLookupType::{Simple, Ligature, Context, ReverseChaining};


#[derive(Clone, Copy, Debug)]
pub enum SubstitutionChange {
	Merge(usize, usize),
	Split(usize, u16)
}

pub trait SubstitutionFeedback {
	fn merge(&mut self, _target: usize, _source: usize) {}
	fn split(&mut self, _origin: usize, _expansion: u16) {}
	fn alternate<'a>(&mut self, _pos: usize, default: Index<Glyph>, _alternates: IndexList<'a, Glyph>) -> Index<Glyph> {default}
}

fn block_swap<T>(buffer: &mut[T], mut pivot: usize) {
	assert!(pivot <= buffer.len());
	let len = {buffer.len()};
	let mut range = 0..len;
	loop {
		let mut buf = &mut buffer[range.clone()];
		let latter_len = range.end - range.start - pivot;
		if  latter_len > pivot {
			if pivot == 0 {return}
			for i in 0..pivot {
				buf.swap(i, i+pivot);
			}
			range = (range.start+pivot)..range.end;
		} else {
			if pivot == len {return}
			for i in 0..latter_len {
				buf.swap(pivot - latter_len + i, pivot+i);
			}
			range = range.start..(range.start + pivot);
			pivot -= latter_len;
		}
	}
}

#[test]
fn test_block_swap() {
	let mut buf = [0, 1, 2, 3, 4, 5, 6, 7];
	block_swap(&mut buf, 5);
	assert_eq!(buf, [5, 6, 7, 0, 1, 2, 3, 4]);
}

fn apply_in_context<'a, F: SubstitutionFeedback>(lookups: LookupList<'a, GSubLookupProvider>, index: Index<Lookup<GSubLookupProvider>>, context: &mut Vec<(Index<Glyph>, usize)>, pos: usize, context_pos: usize, feedback: &mut F) -> Result<(), CorruptFont<'a>> {
	assert!(context.len() > pos);
	if let Some(lookup) = lookups.get_lookup(index) {
		let (GSubLookup {typ, ref coverages}, _) = lookup?;
		for (subtable_idx, coverage) in coverages.into_iter().enumerate() {
			if let Some(Index(covid, _)) = coverage?.check(context[pos].0)? {
				match typ {
					Simple(Single(subtables)) => {
						let idx_calc = subtables.get(subtable_idx as u16).expect("both ways to access the subtables must have the same length");
						match (idx_calc?, &mut context[pos].0 .0) {
							(Offset(off), glyph) => *glyph = glyph.wrapping_add(off),
							(List(list), glyph) => match list.get(covid) {
								Some(g) => *glyph = g.0,
								None => return Err(CorruptFont(list.0, FontCorruption::CoverageIndexTooHigh))
							}
						}
						return Ok(())
					},
					Simple(Alternate(subtables)) => {
						let list = subtables.get(subtable_idx as u16).expect("both ways to access the subtables must have the same length")?;
						let &mut(ref mut glyph, masked) = &mut context[pos];
						*glyph = feedback.alternate(context_pos + pos + masked, *glyph, list);
						return Ok(())
					},
					Simple(Multiple(subtables)) => {
						let subtable = subtables.get(subtable_idx as u16).expect("both ways to access the subtables must have the same length")?;
						let mut seq = if let Some(s) = subtable.get(covid) {s?} else {return Err(CorruptFont(subtable.data, FontCorruption::CoverageIndexTooHigh))};
						feedback.split(context_pos + pos + context[pos].1, seq.len());
						let pivot = context.len() -  seq.len() as usize - 1;	
						context[pos].0 = if let Some(g) = seq.next() {g} else {return Err(CorruptFont(seq.0, FontCorruption::EmptyMultipleSubstitution))};
						let masked = context[pos].1;
						context.extend(seq.map(|x| (x, masked)));
						block_swap(&mut context[pos+1..], pivot);
						return Ok(())
					},
					Ligature(subtables) => {
						let subtable = subtables.get(subtable_idx as u16).expect("both ways to access the subtables must have the same length")?;
						let lig_set = if let Some(ls) = subtable.get(covid) {ls?} else {return Err(CorruptFont(subtable.data, FontCorruption::CoverageIndexTooHigh))};
						'ligature: for ligature in &lig_set {
							let lig = ligature?;
							let num_components = lig.components.len() as usize;
							if num_components > context.len() - pos {continue 'ligature}
							for (component, (glyf, _)) in lig.components.into_iter().zip(context[pos+1..].iter().cloned()) {
								if component != glyf {continue 'ligature}
							}
							for (p, (_, masked)) in context[pos+1..pos+num_components+1].iter().cloned().enumerate() {
								feedback.merge(context_pos + pos, context_pos+ pos + 1 + p + masked);
							}
							block_swap(&mut context[pos+1..], num_components);
							let new_len = context.len() - num_components;
							context.truncate(new_len);
							return Ok(())
						}
					},
					_ => unimplemented!()
				}
			}
		}
		Ok(())
	} else {Err(CorruptFont(lookups.0, FontCorruption::LookupIndexTooHigh))}
}

const RETEST: bool = true;

pub fn run<'a, G: Iterator<Item=Index<Glyph>>, F: SubstitutionFeedback>(lookups: LookupList<'a, GSubLookupProvider>, index: Index<Lookup<GSubLookupProvider>>, mut glyphs: G, class_def: ClassDef<'a>, feedback: &mut F) -> Result<Vec<Index<Glyph>>, CorruptFont<'a>> {
	let (GSubLookup {typ, coverages}, config) = if let Some(x) = lookups.get_lookup(index) {x?} else {return Err(unimplemented!())};
	let filter = config.glyph_filter(class_def);
	if let Simple(lookup) = typ {
		let mut out = Vec::new();
		'glyph: for glyph in glyphs {
			if filter.is_masked(glyph) {
				out.push(glyph);
				continue
			}
			for (subtable_idx, coverage) in (&coverages).into_iter().enumerate() {
				if let Some(Index(covid, _)) = coverage?.check(glyph)? {
					match lookup {
						Single(subtables) => {
							let idx_calc = subtables.get(subtable_idx as u16).expect("both ways to access the subtables must have the same length");
							match idx_calc? {
								Offset(off) => out.push(Index::new(covid.wrapping_add(off))),
								List(list) => out.push(match list.get(covid) {
									Some(g) => g,
									None => return Err(CorruptFont(list.0, FontCorruption::CoverageIndexTooHigh))
								})
							}
						},
						Alternate(subtables) => {
							let list = subtables.get(subtable_idx as u16).expect("both ways to access the subtables must have the same length")?;
							let pos = out.len();
							out.push(feedback.alternate(pos, glyph, list));
						},
						Multiple(subtables) => {
							let subtable = subtables.get(subtable_idx as u16).expect("both ways to access the subtables must have the same length")?;
							let seq = if let Some(s) = subtable.get(covid) {s?} else {return Err(CorruptFont(subtable.data, FontCorruption::CoverageIndexTooHigh))};
							feedback.split(out.len(), seq.len());
							if seq.len() < 1 {return Err(CorruptFont(seq.0, FontCorruption::EmptyMultipleSubstitution))}
							for g in seq {out.push(g)}
						}
					}
					continue 'glyph
				}
			}
		}
		return Ok(out)
	}
	if let ReverseChaining(lookup) = typ {
		let mut out = Vec::with_capacity(glyphs.size_hint().0);
		let mut unmasked = Vec::new();
		for glyph in glyphs {
			if !filter.is_masked(glyph) {
				unmasked.push(out.len());
			}
			out.push(glyph);
		}
		let mut lookbehind = Vec::with_capacity(unmasked.len());
		while let Some(idx) = unmasked.pop() {
			'subtable: for (subtable_idx, coverage) in (&coverages).into_iter().enumerate() {
				if let Some(Index(covid, _)) = coverage?.check(out[idx])? {
					let subtable = lookup.get(subtable_idx as u16).expect("both ways to access the subtables must have the same length")?;
					for (a, b) in subtable.lookahead.into_iter().zip(unmasked.iter().rev().map(|&i| out[i])) {
						if let None = a?.check(b)? {continue 'subtable}
					}
					for (a, b) in subtable.lookbehind.into_iter().zip(lookbehind.iter().rev().map(|&i| out[i])) {
						if let None = a?.check(b)? {continue 'subtable}
					}
					if let Some(glyph) = subtable.substitutes.get(covid) {
						out[idx] = glyph;
						if !(RETEST && filter.is_masked(glyph)) {
							lookbehind.push(idx);
						}
					} else {return Err(CorruptFont(subtable.substitutes.0, FontCorruption::CoverageIndexTooHigh))}
				}
			}
		}
		return Ok(out)
	}
	let mut ensure_enough_lookahead = move|lookahead: &mut VecDeque<(Index<Glyph>, usize)>, masked_glyphs: &mut VecDeque<Index<Glyph>>, len: usize| {
		while lookahead.len() < len {
			let mut masked = 0;
			loop {
				if let Some(g) = glyphs.next() {
					if filter.is_masked(g) {
						masked_glyphs.push_back(g);
						masked += 1;
					} else {
						lookahead.push_back((g, masked));
						break;
					}
				} else {return}
			}
		}
	};
	match typ {
		Simple(_) | ReverseChaining(_) => unreachable!(),
		Ligature(lookup) => {
			let mut out = Vec::new();
			let mut masked_glyphs = VecDeque::new();
			let mut lookahead = VecDeque::new();
			let mut skip_last = 0;
			'glyph: loop {
				ensure_enough_lookahead(&mut lookahead, &mut masked_glyphs, 1);
				let (glyph, cur_skip) = match lookahead.pop_front() {
					Some(x) => x,
					None => break
				};
				for _ in 0..cur_skip+skip_last {out.push(masked_glyphs.pop_front().unwrap())}
				skip_last = 0;
				'subtable: for (subtable_idx, coverage) in (&coverages).into_iter().enumerate() {
					if let Some(Index(covid, _)) = coverage?.check(glyph)? {
						let subtable = lookup.get(subtable_idx as u16).expect("both ways to access the subtables must have the same length")?;
						let lig_set = if let Some(ls) = subtable.get(covid) {ls?} else {return Err(CorruptFont(subtable.data, FontCorruption::CoverageIndexTooHigh))};
						'ligature: for ligature in &lig_set {
							let lig = ligature?;
							let num_components = lig.components.len() as usize;
							ensure_enough_lookahead(&mut lookahead, &mut masked_glyphs, num_components);
							if lookahead.len() < num_components {continue 'ligature}
							for (component, (glyf, _)) in lig.components.into_iter().zip(lookahead.iter().cloned()) {
								if component != glyf {continue 'ligature}
							}
							for (_, skp) in lookahead.drain(..num_components) {
								skip_last += skp;
								feedback.merge(out.len(), out.len() + skp + 1);
							}
							out.push(lig.ligature);
							continue 'glyph
						}
					}
				}
				out.push(glyph);
			}
			while let Some(g) = masked_glyphs.pop_front() {out.push(g)}
			Ok(out)
		},
		Context(lookup) => {
			let mut out = Vec::new();
			let mut lookbehind = if let ContextSubtableProvider::Chaining = lookup.provider {Some(Vec::new())} else {None};
			let mut masked_glyphs = VecDeque::new();
			let mut lookahead = VecDeque::new();
			'glyph: while let Some((glyph, skip)) = {
				ensure_enough_lookahead(&mut lookahead, &mut masked_glyphs, 1);
				lookahead.pop_front()
			} {
				let mut context_match = None;
				'subtable: for (subtable_idx, coverage) in (&coverages).into_iter().enumerate() {
					if let Some(Index(covid, _)) = coverage?.check(glyph)? {
						match lookup.get(subtable_idx as u16).expect("both ways to access the subtables must have the same length")? {
							ContextSubtable::NonCoverage {classes, sub_rules} => {
								let sub_rule_set = if let Some((ctx_classes, _, _)) = classes {
									let GlyphClass(class) = ctx_classes.classify(glyph);
									if let Some(sr) = sub_rules.get(class) {sr?} else {return Err(CorruptFont(sub_rules.data, unimplemented!()))}
								} else {
									if let Some(sr) = sub_rules.get(covid) {sr?} else {return Err(CorruptFont(sub_rules.data, FontCorruption::CoverageIndexTooHigh))}
								};
								'sub_rule: for sub_rule_ in &sub_rule_set {
									let sub_rule = sub_rule_?;
									let ctx_len = sub_rule.context.len() as usize;
									let forward_len = ctx_len + sub_rule.lookahead.len() as usize;
									ensure_enough_lookahead(&mut lookahead, &mut masked_glyphs, forward_len);
									if lookahead.len() < forward_len {continue 'sub_rule}
									let mut iter = lookahead.iter();
									for (a, &(b, _)) in sub_rule.context.into_iter().zip(&mut iter) {
										let mapped = if let Some((ctx_classes, _, _)) = classes {ctx_classes.classify(b).0} else {b.0};
										if a.0 != mapped {continue 'sub_rule}
									}
									for (a, &(b, _)) in sub_rule.context.into_iter().zip(&mut iter) {
										let mapped = if let Some((_, la_classes, _)) = classes {la_classes.classify(b).0} else {b.0};
										if a.0 != mapped {continue 'sub_rule}
									}
									if let &Some(ref lb) = &lookbehind {
										if lb.len() < sub_rule.lookbehind.len() as usize {continue 'sub_rule}
										for (a, &b) in sub_rule.context.into_iter().zip(lb.iter().rev()) {
											let mapped = if let Some((_, _, lb_classes)) = classes {lb_classes.classify(b).0} else {b.0};
											if a.0 != mapped {continue 'sub_rule}
										}
									}
									context_match = Some((ctx_len, sub_rule.subst_records));
									break 'subtable
								}
							},
							ContextSubtable::Coverage {
								context: ctx,
								lookahead: la_,
								lookbehind: lb_,
								subst_records
							} => {
								let ctx_len = ctx.len() as usize;
								let forward_len = ctx_len + if let Some(x) = la_ {x.len() as usize} else {0};
								ensure_enough_lookahead(&mut lookahead, &mut masked_glyphs, forward_len);
								if lookahead.len() < forward_len {continue 'subtable}
								let mut iter = lookahead.iter();
								for (a, &(b, _)) in ctx.into_iter().zip(&mut iter) {
									if let None = a?.check(b)? {continue 'subtable}
								}
								if let Some(la) = la_ {for (a, &(b, _)) in la.into_iter().zip(&mut iter) {
									if let None = a?.check(b)? {continue 'subtable}
								}}
								if let (&Some(ref lookb), Some(lb)) = (&lookbehind, lb_) {
									if lookb.len() < lb.len() as usize {continue 'subtable}
									for (a, &b) in lb.into_iter().zip(lookb.iter().rev()) {
										if let None = a?.check(b)? {continue 'subtable}
									}
								}
								context_match = Some((ctx_len, subst_records));
								break 'subtable
							}
						}
					}
				}
				if let Some((ctx_len, subst_records)) = context_match {
					let mut context = vec![(glyph, skip)];
					let mut masked = skip;
					for _ in 0..ctx_len {
						let (g, m) = lookahead.pop_front().unwrap();
						masked += m;
						context.push((g, masked));
					}
					for record in subst_records {
						apply_in_context(lookups, Index(record.lookup, std::marker::PhantomData), &mut context, record.pos as usize, out.len(), feedback)?;
					}
					let mut masked2 = 0;
					for (g, m) in context {
						for _ in 0..m-masked2 {out.push(masked_glyphs.pop_front().unwrap())}
						masked2 = m;
						match (&mut lookbehind, (RETEST && filter.is_masked(g))) {
							(&mut Some(ref mut lb), false) => lb.push(g),
							_ => {}
						}
						out.push(g);
					}
				} else {
					out.push(glyph);
				}
			}
			Ok(out)
		}
	}
}


