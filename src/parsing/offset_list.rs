use std::ops::Range;
use std::fmt::{Debug, Formatter, Error};

use super::{CorruptFont, TableTooShort, OffsetOutOfBounds, read_u16, TableProvider};

#[derive(Clone, Copy)]
pub struct OffsetList<'a, T> {
	pub data: &'a[u8],
	pub provider: T
}

impl<'a, T: TableProvider<'a>> OffsetList<'a, T> {
	pub fn new(data: &'a[u8], provider: T) -> Result<Self, CorruptFont<'a>> {
		let num_offsets = match read_u16(&data[provider.list_offset()..]) {Some(x) => x as usize, None => return Err(CorruptFont(data, TableTooShort))};
		check_len!(data, provider.offsets_offset() + num_offsets*2);
		Ok(OffsetList {data: data, provider: provider})
	}
	pub fn len(&self) -> u16 {read_u16(&self.data[self.provider.list_offset()..]).unwrap()}
	pub fn get(&self, idx: u16) -> Option<Result<T::Table, CorruptFont<'a>>> {
		if idx >= self.len() {return None}
		let offset = read_u16(&self.data[self.provider.offsets_offset()+2*idx as usize..]).unwrap() as usize;
		Some(if self.data.len() >= offset {
			self.provider.construct(&self.data[offset..])
		} else {
			Err(CorruptFont(self.data, OffsetOutOfBounds))
		})
	}
}

impl<'t, 'a: 't, T: TableProvider<'a>> IntoIterator for &'t OffsetList<'a, T> {
	type Item = Result<T::Table, CorruptFont<'a>>;
	type IntoIter = OffsetIterator<'t, 'a, T>;
	fn into_iter(self) -> Self::IntoIter {OffsetIterator {rng: 0..self.len(), table: self}}
}
pub struct OffsetIterator<'t, 'a: 't, T: TableProvider<'a>> {
	rng: Range<u16>,
	table: &'t OffsetList<'a, T>
}
impl<'t, 'a: 't, T: TableProvider<'a>> Iterator for OffsetIterator<'t, 'a, T> {
	type Item = Result<T::Table, CorruptFont<'a>>;
	fn next(&mut self) -> Option<Self::Item> {
		self.rng.next().and_then(|idx| self.table.get(idx))
	}
	fn size_hint(&self) -> (usize, Option<usize>) {
		let len = self.table.len() as usize;
		(len, Some(len))
	}
}
impl<'t, 'a: 't, T: TableProvider<'a>> ExactSizeIterator for OffsetIterator<'t, 'a, T> {}
impl<'t, 'a: 't, T: TableProvider<'a>> DoubleEndedIterator for OffsetIterator<'t, 'a, T> {
	fn next_back(&mut self) -> Option<Self::Item> {
		match self.rng.next_back() {
			None => None,
			Some(_) => self.table.get(self.rng.end)
		}
	}
}

impl<'a, T: TableProvider<'a>> Debug for OffsetList<'a, T> where T::Table: Debug {
	fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
		f.debug_list().entries(self).finish()
	}
}

#[derive(Clone, Copy)]
pub struct OffsetListProvider<T>(pub T);
impl<'a, T: TableProvider<'a>> TableProvider<'a> for OffsetListProvider<T> {
	type Table = OffsetList<'a, T>;
	fn construct(&self, data: &'a[u8]) -> Result<Self::Table, CorruptFont<'a>> {OffsetList::new(data, self.0)}
}
