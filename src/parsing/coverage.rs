use super::{read_u16, Glyph, CorruptFont};
use super::FontCorruption::{ReservedFeature, InvalidRange, WrappingCoverageIndex};
use super::index_list::{Index, Indexed};
use super::search::{AutoSearch, SearchStrategy};

fn read_range(range: &[u8]) -> Result<(u16, u16, u16), CorruptFont> {
	let last = read_u16(&range[2..]).unwrap();
	let first = read_u16(range).unwrap();
	if last < first {return Err(CorruptFont(&range[..4], InvalidRange))}
	let offset = read_u16(&range[4..]).unwrap();
	if 0xffff-(last-first) < offset {return Err(CorruptFont(range, WrappingCoverageIndex))}
	Ok((first, last, offset))
}

#[derive(Clone, Copy)]
pub enum Coverage<'a>{
	Single(&'a[u8]),
	Range(&'a[u8])
}
impl<'a> Coverage<'a> {
	pub fn new(data: &'a[u8]) -> Result<Coverage<'a>, CorruptFont<'a>> {
		check_len!(data, 4);
		match read_u16(data).unwrap() {
			ver @ 1 | ver @ 2 => {
				let len = read_u16(&data[2..]).unwrap();
				match ver {
					1 => {check_len!(data, len as usize*2); Ok(Coverage::Single(&data[4..][..len as usize*2]))},
					2 => {check_len!(data, len as usize*6); Ok(Coverage::Range(&data[4..][..len as usize*6]))},
					_ => unreachable!()
				}
			},
			_ => Err(CorruptFont(data, ReservedFeature))
		}
	}
	pub fn check(&self, Index(glyph, _): Index<Glyph>) -> Result<Option<Index<CoveredGlyph>>, CorruptFont<'a>> {
		let (data, step) = match self {
			&Coverage::Single(data) => (data, 2),
			&Coverage::Range(data) => (data, 6)
		};
		match AutoSearch::new(data.len()).search(0..(data.len()/step) as u16, &mut move|i|Ok(read_u16(&data[*i as usize*step..]).unwrap().cmp(&glyph))) {
			Ok(i) => Ok(Some(Index::new(if step == 6 {read_u16(&data[i as usize*6+4..]).unwrap()} else {i}))),
			Err(Ok(i)) => {
				if i as usize >= data.len()/step {return Ok(None)}
				let range = &data[i as usize*step..][..step];
				if step == 2 {return Ok(None)}
				let (first, last, offset) = try!(read_range(range));
				Ok(if last >= glyph {
					Some(Index::new(glyph-first+offset))
				} else {
					None
				})
			},
			Err(Err((_, CorruptFont(..)))) => unreachable!()
		}
	}
}

use std::fmt::{Debug, Formatter, Error};
impl<'a> Debug for Coverage<'a> {
	fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
		match *self {
			Coverage::Single(data) => f.debug_list().entries(data.chunks(2).map(|d| read_u16(d).unwrap())).finish(),
			Coverage::Range(data) => f.debug_set().entries(
				data.chunks(6).map(|d| (
					read_u16(d).unwrap()..read_u16(&d[2..]).unwrap(),
					read_u16(&d[4..]).unwrap())
				)
			).finish()
		}
	}
}

#[derive(Clone, Copy)]
pub struct CoveredGlyph;
impl Indexed for CoveredGlyph {} 
