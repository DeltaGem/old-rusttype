use std::error::Error;

#[derive(Clone, Copy, Debug)]
pub enum FontCorruption {
	Unimplemented,
	ReservedFeature,
	TableTooShort,
	OffsetOutOfBounds,
	IncorrectDfltScript,
	CmapInvalidSegmentCount,
	OddSegsX2,
	CmapMissingGuard,
	NoCmap,
	UnknownTableVersion,
	InvalidRange,
	WrappingCoverageIndex,
	CoverageIndexTooHigh,
	EmptyMultipleSubstitution,
	LookupIndexTooHigh,
	ZeroLengthContext
}
use FontCorruption::*;

#[derive(Clone, Copy)]
pub struct CorruptFont<'a>(pub &'a[u8], pub FontCorruption);

impl<'a> Error for CorruptFont<'a> {
	fn description(&self) -> &str {match self.1 {
		Unimplemented => "The font uses a feature that is not implemented",
		ReservedFeature => "A reserved field differed from the default value",
		TableTooShort => "Unexpected end of table",
		OffsetOutOfBounds => "An Offset pointed outside of the respective table",
		IncorrectDfltScript => "'DFLT' script with missing DefaultLangSys or LangSysCount ≠ 0",
		CmapInvalidSegmentCount => "The segment count in the character mapping is invalid",
		OddSegsX2 => "The doubled segment count in the character mapping is not an even number",
		CmapMissingGuard => "The character mapping is missing a guard value",
		NoCmap => "No character mapping found",
		UnknownTableVersion => "The font uses a table version that is not recognised",
		InvalidRange => "Invalid index range (last < first) found in font",
		WrappingCoverageIndex => "Index could wrap in Coverage Range",
		CoverageIndexTooHigh => "A Coverage table returned an index that was beyond the lookup table can deal with",
		EmptyMultipleSubstitution => "Multiple Substitution with an empty sequence",
		LookupIndexTooHigh => "A contextual rule referred to a lookup index that does not exist",
		ZeroLengthContext => "A contextual rule with zero-length context"
	}}
}
impl<'a> ::std::fmt::Display for CorruptFont<'a> {
	fn fmt(&self, f: &mut ::std::fmt::Formatter) -> Result<(), ::std::fmt::Error> {
		write!(f, "{}", self.description())
	}
}
impl<'a> ::std::fmt::Debug for CorruptFont<'a> {
	fn fmt(&self, f: &mut ::std::fmt::Formatter) -> Result<(), ::std::fmt::Error> {
		write!(f, "{}", self)
	}
}
