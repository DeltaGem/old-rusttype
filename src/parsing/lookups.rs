use std::marker::PhantomData;

use super::{CorruptFont, TableTooShort, ReservedFeature, IncorrectDfltScript, OffsetOutOfBounds, read_u16, GlyphFilterConfig};
use super::index_list::{Index, Indexed, PodList};
use super::tag_offset_list::{Tag, Tagged, TagOffsetList, TagListTable};
use super::search::{AutoSearch, SearchStrategy};

pub struct LangSysTable<'a, L>(&'a[u8], L);

impl<'a, L: LookupProvider<'a>> LangSysTable<'a, L> {
	pub fn new(data: &'a[u8], provider: L) -> Result<LangSysTable<'a, L>, CorruptFont<'a>> {
		check_len!(data, 6);
		if read_u16(data).unwrap() != 0 {return Err(CorruptFont(data, ReservedFeature))}
		let num_features = read_u16(&data[4..]).unwrap();
		check_len!(data, num_features as usize*2 + 6);
		Ok(LangSysTable(&data[2..num_features as usize*2 + 6], provider))
	}
	pub fn num_features(&self) -> u16 {(self.0.len() / 2 - 2) as u16}
	pub fn required_feature(&self) -> Option<Index<Feature<L>>> {
		let res = read_u16(self.0).unwrap();
		if res == 0xffff {None} else {Some(Index::new(res))}
	}
	pub fn get_feature(&self, idx: u16) -> Option<Index<Feature<L>>> {read_u16(&self.0[4 + idx as usize*2..]).map(|x| Index::new(x))}
	pub fn features(&self) -> PodList<'a, Feature<L>> {PodList(&self.0[4..], Feature(self.1))}
	pub fn empty(provider: L) -> Self {LangSysTable(&DEFAULT_LANGSYS_TABLE, provider)}
}

static DEFAULT_LANGSYS_TABLE: [u8; 4] = [255, 255, 0, 0];

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct LangSys<L>(L);
pub type ScriptTable<'a, L> = TagOffsetList<'a, LangSys<L>>;
impl<'a, L: LookupProvider<'a>> TagListTable<'a> for LangSys<L> {
	type Table = LangSysTable<'a, L>;
	fn header_length(&self) -> usize {2}
	fn new(&self, data: &'a[u8], _tag: Tag<LangSys<L>>) -> Result<Self::Table, CorruptFont<'a>> {LangSysTable::new(data, self.0)}
}
impl<'a, L: LookupProvider<'a>> Tagged for LangSys<L> {}
impl<'a, L: LookupProvider<'a>> Indexed for LangSys<L> {}

impl<'a, L: LookupProvider<'a>> ScriptTable<'a, L> {
	pub fn new(data: &'a[u8], provider: L) -> Result<ScriptTable<'a, L>, CorruptFont<'a>> {ScriptTable::new_list(data, LangSys(provider))}
	pub fn default_lang_sys(&self) -> Result<LangSysTable<'a, L>, CorruptFont<'a>> {
		let offset = read_u16(self.data).unwrap() as usize;
		if offset == 0 {
			Ok(LangSysTable::empty(self.provider.0))
		} else {
			if self.data.len() < offset {return Err(CorruptFont(self.data, OffsetOutOfBounds))}
			LangSysTable::new(&self.data[offset..], self.provider.0)
		}
	}
	pub fn validate_dflt(&self) -> Result<(), CorruptFont<'a>> {
		if read_u16(self.data).unwrap() != 0 && self.num_tables() == 0 {
			Ok(())
		} else {
			Err(CorruptFont(self.data, IncorrectDfltScript))
		}
	}
}


#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Script<L>(L);
pub type ScriptList<'a, L> = TagOffsetList<'a, Script<L>>;
impl<'a, L: LookupProvider<'a>> TagListTable<'a> for Script<L> {
	type Table = ScriptTable<'a, L>;
	fn new(&self, data: &'a[u8], _: Tag<Self>) -> Result<Self::Table, CorruptFont<'a>> {ScriptTable::new(data, self.0)}
}
impl<'a, L: LookupProvider<'a>> Tagged for Script<L> {}
impl<'a, L: LookupProvider<'a>> Indexed for Script<L> {}

impl<'a, L: LookupProvider<'a>> ScriptList<'a, L> {
	pub fn new(data: &'a[u8], provider: L) -> Result<ScriptList<'a, L>, CorruptFont<'a>> {ScriptList::new_list(data, Script(provider))}
	pub fn features_for(&self, selector: Option<(Tag<Script<L>>, Option<Tag<LangSys<L>>>)>) -> Result<LangSysTable<'a, L>, CorruptFont<'a>> {
		let search = AutoSearch::new(self.num_tables() as usize*6);
		if let Some((script, lang_sys_opt)) = selector {
			match search.search(0..self.num_tables(), &mut move|&i| Ok(self.tag(Index::new(i)).unwrap().0.cmp(&script.0))) {
				Ok(idx) => {
					let script_table = try!(self.table(Index::new(idx)).unwrap());
					if let Some(_) = lang_sys_opt {
						unimplemented!()
					} else {
						return script_table.default_lang_sys()
					}
				},
				Err(Ok(_)) => {println!("default");return Ok(LangSysTable::empty(self.provider.0))},
				Err(Err((_, e))) => return Err(e)
			}
		}
		match search.search(0..self.num_tables(), &mut move|&i| Ok(self.tag(Index::new(i)).unwrap().0.cmp(&DFLT_TAG))) {
			Ok(i) => {
				let script_table = try!(self.table(Index::new(i)).unwrap());
				try!(script_table.validate_dflt());
				script_table.default_lang_sys()
			},
			Err(Ok(_)) => Ok(LangSysTable::empty(self.provider.0)),
			Err(Err((_, e))) => Err(e)
		}
	}
}

static DFLT_TAG: u32 = 0x44464c54;

#[derive(Clone, Copy)]
pub struct NameString;
impl Indexed for NameString {}

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Feature<L>(L);
pub type FeatureList<'a, L> = TagOffsetList<'a, Feature<L>>;
impl<'a, L: LookupProvider<'a>> TagListTable<'a> for Feature<L> {
	type Table = FeatureTable<'a, L>;
	fn new(&self, data: &'a[u8], tag: Tag<Self>) -> Result<Self::Table, CorruptFont<'a>> {FeatureTable::new(data, tag)}
}
impl<'a, T: LookupProvider<'a>> Tagged for Feature<T> {}
impl<'a, T: LookupProvider<'a>> Indexed for Feature<T> {}

impl<'a, L: LookupProvider<'a>> FeatureList<'a, L> {
	pub fn new(data: &'a[u8], provider: L) -> Result<FeatureList<'a, L>, CorruptFont<'a>> {FeatureList::new_list(data, Feature(provider))}
}

pub struct FeatureTable<'a, L: LookupProvider<'a>>{
	pub lookups: PodList<'a, Lookup<L>>,
	pub params: FeatureParams<'a>,
	pub phantom: PhantomData<L>
}
pub enum FeatureParams<'a> {
	Simple,
	/// e. g. Stylistic Sets ('ss01'…'ss20')
	Labelled(Index<NameString>),
	Unsupported(&'a[u8])
}
impl<'a, L: LookupProvider<'a>> FeatureTable<'a, L> {
	fn new(data: &'a[u8], _: Tag<Feature<L>>) -> Result<FeatureTable<'a, L>, CorruptFont<'a>> {
		check_len!(data, 4);
		let params_offset = read_u16(data).unwrap();
		let params = if params_offset != 0 {
			// TODO: dispatch based on tag
			FeatureParams::Unsupported(&data[params_offset as usize..])
		} else {
			FeatureParams::Simple
		};
		let len = read_u16(&data[2..]).unwrap();
		check_len!(data, len as usize*2+4);
		Ok(FeatureTable {
			lookups: PodList(&data[4..len as usize*2+4], Lookup(PhantomData)),
			params: params,
			phantom: PhantomData
		})
	}
}

pub trait LookupProvider<'a>: 'static + Copy {
	type Lookup;
	fn new(data: &'a[u8]) -> Result<Self::Lookup, CorruptFont<'a>>;
}

#[derive(Clone, Copy)]
pub struct Lookup<L>(PhantomData<L>);
impl<'a, L: LookupProvider<'a>> Indexed for Lookup<L> {}

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct LookupList<'a, L>(pub &'a[u8], L);
impl<'a, L: LookupProvider<'a>> LookupList<'a, L> {
	pub fn new(data: &'a[u8], provider: L) -> Result<LookupList<'a, L>, CorruptFont<'a>> {
		check_len!(data, 2);
		let res = LookupList(data, provider);
		check_len!(data, 2+res.len() as usize*2);
		Ok(res)
	}
	fn len(&self) -> u16 {read_u16(self.0).unwrap()}
	pub fn get_lookup(self, Index(idx, _): Index<Lookup<L>>) -> Option<Result<(L::Lookup, GlyphFilterConfig), CorruptFont<'a>>> {
		if idx >= self.len() {return None}
		let offset = read_u16(&self.0[2+idx as usize*2..]).unwrap();
		Some(if offset as usize > self.0.len() {
			Err(CorruptFont(self.0, OffsetOutOfBounds))
		} else {
			let lookup_table = &self.0[offset as usize..];
			if lookup_table.len() < 6 {return Some(Err(CorruptFont(lookup_table, TableTooShort)))}
			let flags = read_u16(&lookup_table[2..]).unwrap();
			let use_mfs = flags & 16 == 0;
			let num_subtables = read_u16(&lookup_table[4..]).unwrap() as usize;
			if lookup_table.len() < 6 + (2*num_subtables + if use_mfs {2} else {0}) {return Some(Err(CorruptFont(lookup_table, TableTooShort)))}
			let lookup = match L::new(lookup_table) {Ok(x) => x, Err(e) => return Some(Err(e))};
			Ok((lookup, GlyphFilterConfig {
				base_glyphs: flags & 2 == 0,
				ligatures: flags & 4 == 0,
				marks: if flags & 8 == 0 {
					Some(((flags >> 8) as u8, if use_mfs {
						Some(Index::new(read_u16(&lookup_table[6+2*num_subtables..]).unwrap()))
					} else {None}))
				} else {
					None
				}
			}))
		})
	}
}
