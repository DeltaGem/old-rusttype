use super::{read_u16, read_u32, CorruptFont};
use super::FontCorruption::ReservedFeature;

#[derive(PartialEq, Clone, Copy)]
pub struct TableDirectory<'a>{
	data: &'a[u8],
	num_tables: u16
}
impl<'a> TableDirectory<'a> {
	pub fn new(data: &[u8]) -> Result<TableDirectory, CorruptFont> {
		check_len!(data, 12);
		let version = read_u32(data);
		if version != Some(0x10000) && version != Some(0x4f54544f) {return Err(CorruptFont(data, ReservedFeature))}
		let num_tables = read_u16(&data[4..]).unwrap();
		check_len!(data, num_tables as usize*16 + 12);
		Ok(TableDirectory {
			data: data,
			num_tables: num_tables
		})
	}
	pub fn find(&self, start: u16, label: u32) -> Option<(u16, &'a[u8])> {
		for pos_ in start..self.num_tables {
			let pos = pos_ as usize;
			if let Some(candidate) = read_u32(&self.data[12+16*pos..]) {
				if candidate == label {
					let start = read_u32(&self.data[12+16*pos+8..]).unwrap() as usize;
					return Some((pos as u16, &self.data[start..read_u32(&self.data[12+16*pos+12..]).unwrap() as usize+start]));
				} else if candidate > label {
					return None
				}
			} else {return None}
		}
		None
	}
	pub fn directory_range(&self) -> &[u8] {&self.data[12..12+16*self.num_tables as usize]}
} 
