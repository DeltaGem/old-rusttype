use super::{read_u16, read_u32, CorruptFont, Glyph};
use super::FontCorruption::{TableTooShort, OddSegsX2, CmapInvalidSegmentCount, CmapMissingGuard, Unimplemented};
use super::index_list::Index;
use super::search::{SearchStrategy, AutoSearch};

#[derive(PartialEq, Clone, Copy)]
pub struct CMap<'otf>(Encoding<'otf>);

impl<'otf> CMap<'otf> {pub fn lookup(&self, c: char) -> Option<Index<Glyph>> {self.0.lookup(c)}}

#[derive(PartialEq, Clone, Copy)]
enum Encoding<'a> {
	Fmt4 {
		end: &'a[u8],
		start: &'a[u8],
		delta: &'a[u8],
		crazy_indexing_part: &'a[u8]
	},
	Fmt12(&'a[u8])
}

impl<'a> Encoding<'a> {
	pub fn lookup(&self, c: char) -> Option<Index<Glyph>> {
		match *self {
			Encoding::Fmt4 {end, start, delta, crazy_indexing_part: range_offset} => {
				if c as u32 > 0xffff {return Some(Index::new(0))}
				let mut range = 0..end.len()/2;
				while range.start != range.end {
					let pivot = ((range.end - range.start) & !1) + range.start*2;
					let pivot_val = read_u16(&end[pivot..]).unwrap();
					range = if pivot_val < c as u16 {
						pivot/2+1..range.end
					} else {
						range.start..pivot/2
					};
				}
				let seg_offset = range.start*2;
				let block_start = read_u16(&start[seg_offset..]).unwrap();
				if block_start > c as u16 {return Some(Index::new(0))}
				return Some(Index::new((read_u16(&delta[seg_offset..]).unwrap()).wrapping_add({
					let offset = read_u16(&range_offset[seg_offset..]).unwrap();
					if offset == 0 {
						c as u16
					} else {	// this path is untested because the spec is really weird and I've never seen it used
						let res = read_u16(&range_offset[seg_offset+(offset as usize &!1)+(c as usize - block_start as usize)..]).unwrap();
						if res == 0 {
							return Some(Index::new(0))
						} else {
							res
						}
					}
				})))
			},
			Encoding::Fmt12(data) => match AutoSearch::new(data.len()).search(0..data.len()/12, &mut move|&i| Ok(read_u32(&data[12*i..]).unwrap().cmp(&(c as u32)))) {
				Ok(i) => read_u32(&data[8+12*i..]).map(|i| Index::new(i as u16)),
				Err(Ok(i)) => {
					if i == data.len()/12 {return Some(Index::new(0))}
					let _first = read_u32(&data[12*i..]).unwrap();
					let last = read_u32(&data[4+12*i..]).unwrap();
					if (c as u32) <= last {
						read_u32(&data[8+12*i..]).map(|i| Index::new(i.wrapping_add(c as u32).wrapping_sub(last) as u16))
					} else {
						Some(Index::new(0))
					}
				}
				Err(Err((_, ()))) => unreachable!()
			}
		}
	}
}

use std::fmt::{Debug, Formatter, Error};
impl<'a> Debug for CMap<'a> {
	fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {match self.0 {
		Encoding::Fmt12(data) => f.debug_map().entries(
			data.chunks(12).map(|d| (
				read_u32(d).unwrap()..read_u32(&d[4..]).unwrap(),
				read_u32(&d[8..]).unwrap())
			)
		).finish(),
		_ => unimplemented!()
	}}
}

#[allow(non_snake_case)]
pub fn load_enc_table(mut enc: &[u8]) -> Result<CMap, CorruptFont> {
	check_len!(enc, 4);
	let format = read_u16(enc).unwrap();
	match format {
		4 => {
			let len = read_u16(&enc[2..]).unwrap() as usize;
			if len < 14 {check_len!(enc, len);}
			enc = &enc[..len];
			let segsX2 = read_u16(&enc[6..]).unwrap() as usize;
			if segsX2 % 2 != 0 {return Err(CorruptFont(enc, OddSegsX2))}
			if segsX2 < 2 || 4*segsX2 + 16 > len {return Err(CorruptFont(enc, CmapInvalidSegmentCount))}
			let end = &enc[14..14+segsX2];
			if read_u16(&end[segsX2-2..]).unwrap() != 0xffff {return Err(CorruptFont(enc, CmapMissingGuard))}
			Ok(CMap(Encoding::Fmt4 {
				end: end,
				start: &enc[16+segsX2..16+2*segsX2],
				delta: &enc[16+2*segsX2..16+3*segsX2],
				crazy_indexing_part: &enc[16+3*segsX2..]
			}))
		},
		12 => {
			check_len!(enc, 16);
			let len = read_u32(&enc[4..]).unwrap() as usize;
			check_len!(enc, len);
			let num_groups = read_u32(&enc[12..]).unwrap() as usize;
			let end = 16+12*num_groups;
			if len < end {return Err(CorruptFont(enc, TableTooShort))}
			Ok(CMap(Encoding::Fmt12(&enc[16..end])))
		},
		_ => Err(CorruptFont(enc, Unimplemented))
	}
} 
