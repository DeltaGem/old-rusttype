use std::marker::PhantomData;
use std::cmp::{PartialEq, Eq, PartialOrd, Ord, Ordering};
use std::fmt::{Debug, Formatter, Error};
use super::{read_u16, CorruptFont, TableTooShort, TableProvider, PodType};

pub trait Indexed: 'static + Copy {}

#[derive(Clone, Copy)]
pub struct Index<T: Indexed>(pub u16, pub PhantomData<T>);
impl<T: Indexed> Index<T> {pub fn new(x: u16) -> Index<T> {Index(x, PhantomData)}}
impl<T: Indexed> Debug for Index<T> {fn fmt(&self, fmt: &mut Formatter) -> Result<(), Error> {write!(fmt, "#{}", self.0)}}
impl<T: Indexed> PartialEq for Index<T> {fn eq(&self, other: &Index<T>) -> bool {self.0 == other.0}}
impl<T: Indexed> Eq for Index<T> {}
impl<T: Indexed> PartialOrd for Index<T> {fn partial_cmp(&self, other: &Index<T>) -> Option<Ordering> {Some(Ord::cmp(self, other))}}
impl<T: Indexed> Ord for Index<T> {fn cmp(&self, other: &Index<T>) -> Ordering {Ord::cmp(&self.0, &other.0)}}

impl<T: Indexed> PodType for T {
	type Data = Index<T>;
	fn stride(&self) -> usize {2}
	fn read(&self, data: &[u8]) -> Self::Data {Index(read_u16(data).unwrap(), PhantomData)}
}
pub type IndexList<'a, T: Indexed> = PodList<'a, T>;

#[derive(Clone, Copy)]
pub struct PodList<'a, T: PodType>(pub &'a[u8], pub T);
impl<'a, T: PodType> PodList<'a, T> {
	pub fn new(data: &'a[u8], typ: T) -> Result<Self, CorruptFont<'a>> {
		match read_u16(data) {
			Some(len) if data.len() >= 2+ typ.stride()*len as usize => Ok(PodList(&data[2..][..typ.stride()*len as usize], typ)),
			_ => Err(CorruptFont(data, TableTooShort))
		}
	}
	pub fn len(&self) -> u16 {(self.0.len()/self.1.stride()) as u16}
	pub fn get(&self, idx: u16) -> Option<T::Data> {if self.len() <= idx {Some(self.1.read(&self.0[idx as usize*self.1.stride()..][..self.1.stride()]))} else {None}}
}
impl<'a, T: PodType> ExactSizeIterator for PodList<'a, T> {}
impl<'a, T: PodType> Iterator for PodList<'a, T> {
	type Item = T::Data;
	fn next(&mut self) -> Option<T::Data> {
		if self.0.len() < self.1.stride() {
			None
		} else {
			let res = &self.0[..self.1.stride()];
			self.0 = &self.0[2..];
			Some(self.1.read(res))
		}
	}
	fn size_hint(&self) -> (usize, Option<usize>) {(self.len() as usize, Some(self.len() as usize))}
}

impl<'a, T: PodType> Debug for PodList<'a, T> {
	fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
		f.debug_list().entries(self.0.chunks(self.1.stride()).map(|d| self.1.read(d))).finish()
	}
}

impl<'a, T: PodType + Default> Default for PodList<'a, T> {
	fn default() -> Self {PodList(&[], Default::default())}
}

#[derive(Clone, Copy)]
pub struct PodListProvider<T>(pub T);
impl<'a, T: PodType + 'static> TableProvider<'a> for PodListProvider<T> {
	type Table = PodList<'a, T>;
	fn construct(&self, data: &'a[u8]) -> Result<Self::Table, CorruptFont<'a>> {
		PodList::new(data, self.0)
	}
}
