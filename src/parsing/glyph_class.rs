use super::{CorruptFont, read_u16, Glyph, UnknownTableVersion};
use super::search::{AutoSearch, SearchStrategy};
use super::index_list::Index;

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct GlyphClass(pub u16);

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum ClassDef<'a> {
	Lut(u16, &'a[u8]),
	Ranges(&'a[u8])
}
use ClassDef::*;

impl<'a> ClassDef<'a> {
	pub fn new(data: &'a[u8]) -> Result<ClassDef<'a>, CorruptFont<'a>> {
		check_len!(data, 4);
		match read_u16(data).unwrap() {
			1 => {
				check_len!(data, 6);
				let num = read_u16(&data[4..]).unwrap() as usize;
				check_len!(data, 6 + 2*num);
				Ok(Lut(read_u16(&data[2..]).unwrap(), &data[6..][..2*num]))
			}
			2 => {
				let num = read_u16(&data[2..]).unwrap() as usize;
				check_len!(data, 4 + 6*num);
				Ok(Ranges(&data[4..][..6*num]))
			}
			_ => Err(CorruptFont(data, UnknownTableVersion))
		}
	}
	pub fn classify(&self, Index(i, _): Index<Glyph>) -> GlyphClass {
		GlyphClass(match *self {
			Lut(start, table) => if i < start || i-start >= (table.len() / 2) as u16 {
				0
			} else {
				read_u16(&table[(2*(i-start)) as usize..]).unwrap()
			},
			Ranges(table) =>{
				match AutoSearch::new(table.len()).search(0..(table.len()/6), &mut|&x| Ok(read_u16(&table[6*x..]).unwrap().cmp(&i))) {
					Ok(r) => read_u16(&table[6*r+4..]).unwrap(),
					Err(Ok(r)) => if read_u16(&table[6*r-4..]).unwrap() >= i {
						read_u16(&table[6*r-2..]).unwrap()
					} else {0},
					Err(Err((_, ()))) => unreachable!()
				}
			}
		})
	}
}

impl<'a> Default for ClassDef<'a> {
	fn default() -> Self {ClassDef::Ranges(&[])}
}

use std::fmt::{Debug, Formatter, Error};
impl<'a> Debug for ClassDef<'a> {
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), Error> {match self {
		&Lut(start, table) => fmt.debug_map().entries((0..table.len()/2).map(|i| (start as usize+i, read_u16(&table[2*i..]).unwrap()))).finish(),
		&Ranges(table) => fmt.debug_map().entries(
			(0..table.len()/6).map(
				|i| (read_u16(&table[6*i..]).unwrap()..read_u16(&table[6*i+2..]).unwrap(), read_u16(&table[6*i+4..]).unwrap())
			)
		).finish()
	}}
}
