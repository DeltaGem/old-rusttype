macro_rules! check_len {
	($data: expr, $len: expr) => {let data = $data;if data.len() < $len {return Err($crate::CorruptFont(data, $crate::FontCorruption::TableTooShort))}}
}

mod table_dir;
pub use self::table_dir::TableDirectory;

mod cmap;
pub use self::cmap::{CMap, load_enc_table};

mod tag_offset_list;
pub use self::tag_offset_list::Tag;

mod index_list;
use self::index_list::Indexed;
pub use self::index_list::{Index, IndexList, PodList, PodListProvider};

mod coverage;
pub use self::coverage::Coverage;

mod glyph_class;
pub use self::glyph_class::{GlyphClass, ClassDef};

mod lookups;
pub use self::lookups::{FeatureList, LookupList, ScriptList, LookupProvider, Script, LangSys, Lookup, LangSysTable};

mod offset_list;
pub use self::offset_list::{OffsetList, OffsetListProvider}; 

mod search;

mod error;
pub use self::error::{CorruptFont, FontCorruption};
use self::error::FontCorruption::*;

/// contains glyphs and typesetting information
#[derive(PartialEq, Clone, Copy)]
pub struct FontCollection<'a>{
	pub tables: TableDirectory<'a>,
	pub opentype: Option<OpenTypeTables<'a>>
}

/// contains character mapping
#[derive(PartialEq, Clone, Copy)]
pub struct Font<'a>{
	pub cmap: CMap<'a>,
	glyph_src: &'a FontCollection<'a>
}

pub fn read_u32(data: &[u8]) -> Option<u32> {
	if data.len() > 3 {
		Some((data[0] as u32) << 24 | (data[1] as u32) << 16 | (data[2] as u32) << 8 | data[3] as u32)
	} else {
		None
	}
}

pub fn read_u16(data: &[u8]) -> Option<u16> {
	if data.len() > 1 {
		Some((data[0] as u16) << 8 | data[1] as u16)
	} else {
		None
	}
}

pub fn fourcc(tag: u32) -> String {
	let mut s = String::with_capacity(4);
	s.push((tag >> 24) as u8 as char);
	s.push((tag >> 16) as u8 as char);
	s.push((tag >> 8) as u8 as char);
	s.push(tag as u8 as char);
	s
}
fn find_best_cmap(cmap: &[u8]) -> Option<&[u8]> {
	let mut bmp = None;
	for encoding in 0..read_u16(&cmap[2..]).unwrap() as usize {
		let enc_header = &(&cmap[4+8*encoding..])[..8];
		let (plat, enc) = (read_u16(enc_header).unwrap(), read_u16(&enc_header[2..]).unwrap());
		match (plat, enc) {
			(0, 3) | (3, 1) => if let Some(offset) = read_u32(&enc_header[4..]) {bmp=Some(&cmap[offset as usize..]);} else {return None},
			(0, 4) | (3, 10) => return if let Some(offset) = read_u32(&enc_header[4..]) {Some(&cmap[offset as usize..])} else {None},
			_ => {}	// unknown encoding
		}
	}
	bmp
}

pub fn load_font_collection(data: &[u8]) -> Result<FontCollection, CorruptFont> {
	let tables = try!(TableDirectory::new(data));
	println!("#glyphs: {:?}", tables.find(0, MAXP_TAG).and_then(|x|read_u16(&x.1[4..])));
	;
	Ok(FontCollection {
		opentype: if let Some(gdef) = tables.find(0, GDEF_TAG) {
			Some(OpenTypeTables {
				gdef: try!(GDef::new(gdef.1)),
				gsub: if let Some(x) = tables.find(0, GSUB_TAG) {
					Some(try!(GSub::new(x.1)))
				} else {None},
				gpos: None
			})
		} else {None},
		tables: tables
	})
}

pub const MAXP_TAG: u32 = 0x6d617870;
pub const GDEF_TAG: u32 = 0x47444546;
pub const CMAP_TAG: u32 = 0x636d6170;
pub const HHEA_TAG: u32 = 0x68686561;
pub const GSUB_TAG: u32 = 0x47535542;

pub fn load_font<'a>(collection: &'a FontCollection<'a>, _font: &str) -> Result<Font<'a>, CorruptFont<'a>> {
	let (_pos, cmap) = try!(collection.tables.find(0, CMAP_TAG).ok_or(CorruptFont(collection.tables.directory_range(), NoCmap)));
	let best_enc = try!(find_best_cmap(cmap).ok_or(CorruptFont(cmap, NoCmap)));
	let enc = try!(load_enc_table(best_enc));
	Ok(Font {cmap: enc, glyph_src: collection})
}

#[derive(Clone, Copy)]
pub struct Glyph;
impl Indexed for Glyph {}
impl Default for Glyph {fn default() -> Glyph {Glyph}}

#[derive(PartialEq, Clone, Copy)]
pub struct OpenTypeTables<'a> {
	pub gdef: GDef<'a>,
	pub gsub: Option<GSub<'a>>,
	pub gpos: Option<GPos<'a>>
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct GlyphFilterConfig {
	base_glyphs: bool,
	ligatures: bool,
	marks: Option<(u8, Option<Index<MarkFilteringSet>>)>
}

impl GlyphFilterConfig {
	pub fn glyph_filter(self, class_def: ClassDef) -> GDefGlyphFilter {
		GDefGlyphFilter {
			class_def: class_def,
			config: self
		}
	}
}

#[derive(PartialEq, Clone, Copy)]
pub struct MarkFilteringSet;
impl Indexed for MarkFilteringSet {}

pub trait GlyphFilter {
	fn is_masked(&self, idx: Index<Glyph>) -> bool;
}

#[derive(PartialEq, Clone, Copy)]
pub struct GDef<'a> {
	pub class_def: ClassDef<'a>
}

#[derive(Clone, Copy, Debug)]
pub struct GDefGlyphFilter<'a> {
	class_def: ClassDef<'a>,
	config: GlyphFilterConfig
}

impl<'a> GDef<'a> {
	fn new(data: &'a[u8]) -> Result<GDef<'a>, CorruptFont<'a>> {
		if data.len() < 12 {return Err(CorruptFont(data, TableTooShort))}
		let major = read_u16(data).unwrap();
		if major != 1 {return Err(CorruptFont(data, UnknownTableVersion))}
		let class_def_off = read_u16(&data[4..]).unwrap();
		Ok(GDef {
			class_def: if class_def_off == 0 {
				Default::default()
			} else {
				if class_def_off as usize > data.len() {return Err(CorruptFont(data, OffsetOutOfBounds))}
				try!(ClassDef::new(&data[class_def_off as usize..]))
			}
		})
	}
}

impl<'a> GlyphFilter for GDefGlyphFilter<'a> {
	fn is_masked(&self, idx: Index<Glyph>) -> bool {
		match self.class_def.classify(idx) {
			GlyphClass(1) => !self.config.base_glyphs,
			GlyphClass(2) => !self.config.ligatures,
			GlyphClass(3) => unimplemented!(),
			_ => false
		}
	}
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct GSub<'a> {
	pub script_list: ScriptList<'a, GSubLookupProvider>,
	pub feature_list: FeatureList<'a, GSubLookupProvider>,
	pub lookups: LookupList<'a, GSubLookupProvider>
}

impl<'a> GSub<'a> {
	fn new(data: &'a[u8]) -> Result<GSub<'a>, CorruptFont<'a>> {
		check_len!(data, 10);
		if read_u32(data) != Some(0x00010000) {return Err(CorruptFont(data, UnknownTableVersion))}
		let scr_off = read_u16(&data[4..]).unwrap() as usize;
		let feat_off = read_u16(&data[6..]).unwrap() as usize;
		let lut_off = read_u16(&data[8..]).unwrap() as usize;
		if data.len() < scr_off || data.len() < feat_off || data.len() < lut_off {return Err(CorruptFont(data, OffsetOutOfBounds))}
		Ok(GSub {
			script_list: try!(ScriptList::new(&data[scr_off..], GSubLookupProvider)),
			feature_list: try!(FeatureList::new(&data[feat_off..], GSubLookupProvider)),
			lookups: try!(LookupList::new(&data[lut_off..], GSubLookupProvider))
		})
	}
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct GSubLookupProvider;
impl Default for GSubLookupProvider {fn default() -> GSubLookupProvider {GSubLookupProvider}}

#[derive(Clone, Copy, Debug)]
pub struct GSubLookup<'a> {
	pub coverages: OffsetList<'a, SubtableCoverageProvider>,
	pub typ: GSubLookupType<'a>
}

#[derive(Clone, Copy, Debug)]
pub enum GSubLookupType<'a> {
	Simple(SimpleSubstitution<'a>),
	Ligature(OffsetList<'a, LigatureSubtableProvider>),
	Context(OffsetList<'a, ContextSubtableProvider>),
	ReverseChaining(OffsetList<'a, ReverseChainingProvider>)
}

#[derive(Clone, Copy)]
pub enum SubtableCoverageProvider {Normal, Context, ChainingContext}

impl<'a> TableProvider<'a> for SubtableCoverageProvider {
	type Table = Coverage<'a>;
	fn construct(&self, data: &'a[u8]) -> Result<Self::Table, CorruptFont<'a>> {
		check_len!(data, 4);
		let offset = read_u16(&data[match (read_u16(data).unwrap(), *self) {
			(3, SubtableCoverageProvider::Context) => if read_u16(&data[2..]).unwrap() == 0 {
				return Err(CorruptFont(data, FontCorruption::ZeroLengthContext))
			} else {check_len!(data, 8); 6},
			(2, SubtableCoverageProvider::ChainingContext) => {
				let backtrack_len = read_u16(&data[2..]).unwrap() as usize;
				check_len!(data, 6+backtrack_len*2);
				if read_u16(&data[2+backtrack_len*2..]).unwrap() == 0 {return Err(CorruptFont(data, FontCorruption::ZeroLengthContext))}
				4 + backtrack_len*2
			},
			(_, SubtableCoverageProvider::Normal) | (1, _) | (2, _) => 2,
			_ => return Err(CorruptFont(data, FontCorruption::UnknownTableVersion))
		}..]).unwrap();
		if offset as usize > data.len() {return Err(CorruptFont(data, FontCorruption::OffsetOutOfBounds))}
		Coverage::new(&data[offset as usize..])
	}
	fn list_offset(&self) -> usize {4}
}


impl<'a> LookupProvider<'a> for GSubLookupProvider {
	type Lookup = GSubLookup<'a>;
	fn new(data: &'a[u8]) -> Result<Self::Lookup, CorruptFont<'a>> {
		assert!(data.len() >= 6);
		let typ = read_u16(data).unwrap();
		Ok(GSubLookup {
			coverages: OffsetList::new(data, match typ {
				5 => SubtableCoverageProvider::Context,
				6 => SubtableCoverageProvider::ChainingContext,
				_ => SubtableCoverageProvider::Normal
			})?,
			typ: match typ {
				1 => GSubLookupType::Simple(SimpleSubstitution::Single(OffsetList::new(data, SingleSubtableProvider)?)),
				2 => GSubLookupType::Simple(SimpleSubstitution::Multiple(OffsetList::new(data, MultipleSubtableProvider)?)),
				3 => GSubLookupType::Simple(SimpleSubstitution::Alternate(OffsetList::new(data, AlternateSubtableProvider)?)),
				4 => GSubLookupType::Ligature(OffsetList::new(data, LigatureSubtableProvider)?),
				5 => GSubLookupType::Context(OffsetList::new(data, ContextSubtableProvider::Normal)?),
				6 => GSubLookupType::Context(OffsetList::new(data, ContextSubtableProvider::Chaining)?),
				8 => GSubLookupType::ReverseChaining(OffsetList::new(data, ReverseChainingProvider)?),
				_ => unimplemented!()
			}
		})
	}
}

#[derive(Clone, Copy, Debug)]
pub enum SimpleSubstitution<'a> {
	Single(OffsetList<'a, SingleSubtableProvider>),
	Alternate(OffsetList<'a, AlternateSubtableProvider>),
	Multiple(OffsetList<'a, MultipleSubtableProvider>)
}
pub trait TableProvider<'a>: Copy + 'static {
	type Table;
	fn construct(&self, data: &'a[u8]) -> Result<Self::Table, CorruptFont<'a>>;
	fn list_offset(&self) -> usize {0}
	fn offsets_offset(&self) -> usize {self.list_offset() + 2}
}

#[derive(Clone, Copy)]
pub struct SingleSubtableProvider;
impl<'a> TableProvider<'a> for SingleSubtableProvider {
	type Table = GlyphIndexCalculator<'a>;
	fn construct(&self, data: &'a[u8]) -> Result<Self::Table, CorruptFont<'a>> {
		check_len!(data, 6);
		let version = read_u16(data).unwrap();
		Ok(match version {
			1 => GlyphIndexCalculator::Offset(read_u16(&data[4..]).unwrap()),
			2 => GlyphIndexCalculator::List(IndexList::new(&data[4..], Glyph)?),
			_ => return Err(CorruptFont(data, UnknownTableVersion))
		})
	}
	fn list_offset(&self) -> usize {4}
}

#[derive(Clone, Copy, Debug)]
pub enum GlyphIndexCalculator<'a> {
	Offset(u16),
	List(IndexList<'a, Glyph>)
}

#[derive(Clone, Copy)]
pub struct AlternateSubtableProvider;
impl<'a> TableProvider<'a> for AlternateSubtableProvider {
	type Table = IndexList<'a, Glyph>;
	fn construct(&self, data: &'a[u8]) -> Result<Self::Table, CorruptFont<'a>> {
		check_len!(data, 6);
		if read_u16(data).unwrap() != 1 {return Err(CorruptFont(data, UnknownTableVersion))}
		IndexList::new(&data[6..], Glyph)
	}
	fn list_offset(&self) -> usize {4}
}

#[derive(Clone, Copy)]
pub struct MultipleSubtableProvider;
impl<'a> TableProvider<'a> for MultipleSubtableProvider {
	type Table = OffsetList<'a, PodListProvider<Glyph>>;
	fn construct(&self, data: &'a[u8]) -> Result<Self::Table, CorruptFont<'a>> {
		check_len!(data, 6);
		if read_u16(data).unwrap() != 1 {return Err(CorruptFont(data, UnknownTableVersion))}
		OffsetList::new(data, PodListProvider(Glyph))
	}
	fn list_offset(&self) -> usize {4}
}

#[derive(Clone, Copy)]
pub struct LigatureSubtableProvider;
impl<'a> TableProvider<'a> for LigatureSubtableProvider {
	type Table = OffsetList<'a, LigatureSetProvider>;
	fn construct(&self, data: &'a[u8]) -> Result<Self::Table, CorruptFont<'a>> {
		check_len!(data, 6);
		if read_u16(data).unwrap() != 1 {return Err(CorruptFont(data, UnknownTableVersion))}
		OffsetList::new(data, LigatureSetProvider)
	}
	fn list_offset(&self) -> usize {4}
}

#[derive(Clone, Copy)]
pub struct LigatureSetProvider;
impl<'a> TableProvider<'a> for LigatureSetProvider {
	type Table = OffsetList<'a, LigatureProvider>;
	fn construct(&self, data: &'a[u8]) -> Result<Self::Table, CorruptFont<'a>> {OffsetList::new(data, LigatureProvider)}
	fn list_offset(&self) -> usize {4}
}

#[derive(Clone, Copy)]
pub struct LigatureProvider;
impl<'a> TableProvider<'a> for LigatureProvider {
	type Table = LigatureTable<'a>;
	fn construct(&self, data: &'a[u8]) -> Result<Self::Table, CorruptFont<'a>> {
		IndexList::new(&data[2..], Glyph).map(|mut comps| LigatureTable {
			ligature: Index::new(read_u16(data).unwrap()),
			components: {comps.0 = &comps.0[..comps.0.len()-2]; comps}
		})
	}
}

#[derive(Clone, Copy, Debug)]
pub struct LigatureTable<'a> {
	pub ligature: Index<Glyph>,
	pub components: IndexList<'a, Glyph>
}

#[derive(Clone, Copy, Debug)]
pub enum ContextSubtableProvider {Normal, Chaining}
impl<'a> TableProvider<'a> for ContextSubtableProvider {
	type Table = ContextSubtable<'a>;
	fn construct(&self, data: &'a[u8]) -> Result<Self::Table, CorruptFont<'a>> {
		use self::ContextSubtableProvider::*;
		check_len!(data, 6);
		return Ok(match read_u16(data).unwrap() {
			1 => ContextSubtable::NonCoverage {
				classes: None,
				sub_rules: OffsetList::new(data, OffsetListProvider(SubRuleProvider(2, *self)))?
			},
			2 => {
				check_len!(data, 8);
				let ctx_offset = read_u16(&data[if let &Normal = self {4} else {6}..]).unwrap() as usize;
				if data.len() < ctx_offset {return Err(CorruptFont(data, FontCorruption::OffsetOutOfBounds))}
				let ctx_classes = ClassDef::new(&data[ctx_offset..])?;
				let (la_classes, lb_classes) = if let &Chaining = self {
					check_len!(data, 12);
					let la_offset = read_u16(&data[4..]).unwrap() as usize;
					let lb_offset = read_u16(&data[8..]).unwrap() as usize;
					if data.len() < la_offset || data.len() < lb_offset {return Err(CorruptFont(data, FontCorruption::OffsetOutOfBounds))}
					(ClassDef::new(&data[la_offset..])?, ClassDef::new(&data[lb_offset..])?)
				} else {(Default::default(), Default::default())};
				ContextSubtable::NonCoverage {
					classes: Some((ctx_classes, la_classes, lb_classes)),
					sub_rules: OffsetList::new(data, OffsetListProvider(SubRuleProvider(if let &Normal = self {6} else {10}, *self)))?
				}
			},
			3 => if let &Normal = self {
				let ctx_len = read_u16(&data[2..]).unwrap() as usize;
				let subst_len = read_u16(&data[4..]).unwrap() as usize;
				check_len!(data, 6 + ctx_len*2 + subst_len*4);
				ContextSubtable::Coverage {
					context: OffsetList::new(data, CoverageProvider(2, 6))?,
					lookahead: None,
					lookbehind: None,
					subst_records: PodList(&data[6+ctx_len*2..][..subst_len*4], SubstRecType)
				}
			} else {
				let lb = OffsetList::new(data, CoverageProvider(2, 4))?;
				let ctx_offset = 6 + lb.len() as usize*2;
				let ctx = OffsetList::new(data, CoverageProvider(ctx_offset, ctx_offset+2))?;
				let la_offset = ctx_offset + 2 + ctx.len() as usize*2;
				let la = OffsetList::new(data, CoverageProvider(la_offset, la_offset+2))?;
				let subst_offset = la_offset + 2 + la.len() as usize*2;
				check_len!(data, subst_offset + 2);
				let subst_len = read_u16(&data[subst_offset..]).unwrap() as usize;
				check_len!(data, subst_offset + 2 + subst_len*4);
				ContextSubtable::Coverage {
					context: OffsetList::new(&data, CoverageProvider(2, 6))?,
					lookahead: if la.len() > 0 {Some(la)} else {None},
					lookbehind: if lb.len() > 0 {Some(lb)} else {None},
					subst_records: PodList(&data[subst_offset+2..][..subst_len*4], SubstRecType)
				}
			},
			_ => return Err(CorruptFont(data, FontCorruption::UnknownTableVersion))
		})
	}
}

#[derive(Clone, Copy, Debug)]
pub enum ContextSubtable<'a> {
	Coverage {
		context: OffsetList<'a, CoverageProvider>,
		lookahead: Option<OffsetList<'a, CoverageProvider>>,
		lookbehind: Option<OffsetList<'a, CoverageProvider>>,
		subst_records: PodList<'a, SubstRecType>
	},
	NonCoverage {
		classes: Option<(ClassDef<'a>, ClassDef<'a>, ClassDef<'a>)>,
		sub_rules: OffsetList<'a, OffsetListProvider<SubRuleProvider>>
	}
}

#[derive(Clone, Copy, Debug)]
pub struct CoverageProvider(usize, usize);
impl<'a> TableProvider<'a> for CoverageProvider {
	type Table = Coverage<'a>;
	fn construct(&self, data: &'a[u8]) -> Result<Self::Table, CorruptFont<'a>> {
		Coverage::new(data)
	}
	fn list_offset(&self) -> usize {self.0}
	fn offsets_offset(&self) -> usize {self.1}
}

#[derive(Clone, Copy, Debug)]
pub struct SubRuleProvider(usize, pub ContextSubtableProvider);
impl<'a> TableProvider<'a> for SubRuleProvider {
	type Table = SubRule<'a>;
	fn construct(&self, data: &'a[u8]) -> Result<Self::Table, CorruptFont<'a>> {Ok(match self.1 {
		ContextSubtableProvider::Normal => {
			check_len!(data, 4);
			let glyph_count = read_u16(data).unwrap() as usize;
			let subst_count = read_u16(data).unwrap() as usize;
			let subst_offset = 2 + glyph_count*2;
			check_len!(data, subst_offset + subst_count*4);
			SubRule {
				context: PodList(&data[4..][..if let Some(x) = glyph_count.checked_sub(1) {x} else {unimplemented!()}*2], Glyph),
				lookahead: Default::default(),
				lookbehind: Default::default(),
				subst_records: PodList(&data[subst_offset..][..subst_count*4], SubstRecType)
			}
		}
		ContextSubtableProvider::Chaining => {
			let lb = IndexList::new(data, Glyph)?;
			let ctx_offset = 2+lb.len() as usize;
			let mut ctx = IndexList::new(&data[ctx_offset..], Glyph)?;
			let la_offset = ctx_offset + 2 + ctx.len() as usize;
			ctx.0 = &ctx.0[..2];
			let la = IndexList::new(&data[la_offset..], Glyph)?;
			let subst_offset = la_offset + 2 + la.len() as usize;
			check_len!(data, subst_offset);
			let subst_count = read_u16(&data[subst_offset..]).unwrap() as usize;
			check_len!(data, subst_offset + 2 + 4*subst_count);
			SubRule {
				context: ctx,
				lookahead: la,
				lookbehind: lb,
				subst_records: PodList(&data[subst_offset+2..][..4*subst_count], SubstRecType)
			}
		}
	})}
	fn list_offset(&self) -> usize {self.0}
}

#[derive(Clone, Copy, Debug)]
pub struct SubRule<'a> {
	pub context: IndexList<'a, Glyph>,
	pub lookahead: IndexList<'a, Glyph>,
	pub lookbehind: IndexList<'a, Glyph>,
	pub subst_records: PodList<'a, SubstRecType>
}

pub trait PodType: Copy {
	type Data: 'static + Copy + ::std::fmt::Debug;
	fn stride(&self) -> usize;
	fn read(&self, data: &[u8]) -> Self::Data;
}
#[derive(Clone, Copy, Debug)]
pub struct SubstRecord {
	pub lookup: u16, //Index<GSubLookupProvider>,
	pub pos: u16
}
#[derive(Clone, Copy, Debug)]
pub struct SubstRecType;
impl PodType for SubstRecType {
	type Data = SubstRecord;
	fn stride(&self) -> usize {4}
	fn read(&self, data: &[u8]) -> Self::Data {
		let lookup = read_u16(data).unwrap();
		let pos = read_u16(&data[2..]).unwrap();
		SubstRecord {lookup: lookup, pos: pos}
	}
}

#[derive(Clone, Copy, Debug)]
pub struct ReverseChainingProvider;
impl<'a> TableProvider<'a> for ReverseChainingProvider {
	type Table = ReverseChainingSubtable<'a>;
	fn construct(&self, data: &'a[u8]) -> Result<Self::Table, CorruptFont<'a>> {
		check_len!(data, 6);
		let lb = OffsetList::new(data, CoverageProvider(4, 6))?;
		let la_offset = 6 + 2*lb.len() as usize;
		let la = OffsetList::new(data, CoverageProvider(la_offset, la_offset+2))?;
		let sub_offset = la_offset + 2 + 2*la.len() as usize;
		Ok(ReverseChainingSubtable {
			lookahead: la,
			lookbehind: lb,
			substitutes: IndexList::new(&data[sub_offset..], Glyph)?
		})
	}
}

#[derive(Clone, Copy, Debug)]
pub struct ReverseChainingSubtable<'a> {
	pub lookahead: OffsetList<'a, CoverageProvider>,
	pub lookbehind: OffsetList<'a, CoverageProvider>,
	pub substitutes: IndexList<'a, Glyph>
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct GPos<'a>(&'a[u8]);
