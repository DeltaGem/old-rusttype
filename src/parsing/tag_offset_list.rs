use std::marker::PhantomData;
use super::{read_u16, read_u32, CorruptFont};
use super::index_list::{Indexed, Index};

pub trait Tagged {}
pub trait TagListTable<'a>: 'static + Indexed + Tagged + Copy {
	type Table;
	fn header_length(&self) -> usize {0}
	fn new(&self, data: &'a[u8], tag: Tag<Self>) -> Result<Self::Table, CorruptFont<'a>> where Self: Sized;
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct TagOffsetList<'a, T: TagListTable<'a>> {
	pub data: &'a[u8],
	pub provider: T
}

pub struct Tag<T: Tagged>(pub u32, pub PhantomData<T>);
impl<T: Tagged> Tag<T> {pub fn new(v: u32) -> Tag<T> {Tag(v, PhantomData)}}

impl<'a, T: TagListTable<'a>> TagOffsetList<'a, T> {
	pub fn new_list(data: &'a[u8], provider: T) -> Result<TagOffsetList<'a, T>, CorruptFont<'a>> {
		check_len!(data, 2 + provider.header_length());
		let res = TagOffsetList {data: data, provider: provider};
		check_len!(data, res.num_tables() as usize*6 + 2 + res.provider.header_length());
		Ok(res)
	}
	pub fn num_tables(&self) -> u16 {read_u16(&self.data[self.provider.header_length()..]).unwrap()}
	pub fn tag(&self, Index(idx, _): Index<T>) -> Option<Tag<T>> {
		read_u32(&self.data[idx as usize*6+2+self.provider.header_length()..]).map(|x| Tag(x, PhantomData))
	}
	pub fn table(&self, Index(index, _): Index<T>) -> Option<Result<T::Table, CorruptFont<'a>>> {
		let offset_pos = &self.data[index as usize*6 + 6 + self.provider.header_length()..];
		let offset = read_u16(offset_pos).unwrap() as usize;
		if self.data.len() < offset {return None}
		Some(self.provider.new(&self.data[offset..], Tag::new(read_u32(&self.data[index as usize*6+2+ self.provider.header_length()..]).unwrap())))
	}
	pub fn header(&self) -> &[u8] {&self.data[..self.provider.header_length()]}
}
impl<'a, T: TagListTable<'a>> IntoIterator for TagOffsetList<'a, T> {
	type Item = (Tag<T>, Result<T::Table, CorruptFont<'a>>);
	type IntoIter = TagOffsetIterator<'a, T>;
	fn into_iter(self) -> TagOffsetIterator<'a, T> {TagOffsetIterator(self, 0)}
}

#[derive(Clone, Copy)]
pub struct TagOffsetIterator<'a, T: TagListTable<'a>>(TagOffsetList<'a, T>, u16);
impl<'a, T: TagListTable<'a>> Iterator for TagOffsetIterator<'a, T> {
	type Item = (Tag<T>, Result<T::Table, CorruptFont<'a>>);
	fn next(&mut self) -> Option<<Self as Iterator>::Item> {
		if self.1 >= self.0.num_tables() {
			None
		} else {
			self.1 += 1;
			Some((self.0.tag(Index::new(self.1 - 1)).unwrap(), self.0.table(Index::new(self.1 - 1)).unwrap()))
		}
	}
	fn size_hint(&self) -> (usize, Option<usize>) {let res = self.0.num_tables() as usize; (res, Some(res))}
}
impl<'a, T: TagListTable<'a> + Tagged> ExactSizeIterator for TagOffsetIterator<'a, T> {} 
