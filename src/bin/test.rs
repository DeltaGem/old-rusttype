pub extern crate rusttype;
use rusttype::*;
use std::collections::BTreeSet;

fn main() {
	let font_file = include_bytes!("verdana.ttf");//"SourceSansPro-Regular.ttf"); //"FiraSans-Regular.ttf");
	let collection = load_font_collection(font_file).unwrap();
	let font = load_font(&collection, "").unwrap();
	println!("A: {:?} b: {:?} c: {:?}", font.cmap.lookup('A').unwrap(), font.cmap.lookup('b').unwrap(), font.cmap.lookup('c').unwrap());
	println!("{:?}", BasicShaper.shape("Abc", &mut|x|Some(x)));
	let gsub = collection.opentype.unwrap().gsub.unwrap();
	let out = |tag: Tag<LangSys<_>>, langsys: LangSysTable<_>|println!("	{} features: {:?} mandatory, others: {:?}", fourcc(tag.0), langsys.required_feature(), langsys.features().collect::<Vec<_>>());
	for (i, (tag, script)) in gsub.script_list.into_iter().enumerate() {
		println!("{}: {:x} {}", i, tag.0, fourcc(tag.0));
		let script = script.unwrap();
		out(Tag::new(0x20202020), script.default_lang_sys().unwrap());
		for (tag, langsys) in script {
			out(tag, langsys.unwrap());
		}
	}
	let latin = gsub.script_list.features_for(Some((Tag::new(0x6c61746e), None))).unwrap();
	for feat in latin.features() {
		println!("feature: {} => {}", feat.0, fourcc(gsub.feature_list.tag(feat.clone()).unwrap().0));
		for lookup in gsub.feature_list.table(feat).unwrap().unwrap().lookups {
			println!("{:?}", lookup);
		}
	}
	let (mut text, _mapper) = BasicShaper.shape("effizient", &mut|x|font.cmap.lookup(x)).unwrap();
	let lookups: BTreeSet<_> = latin.features().into_iter().filter(|feat| &fourcc(gsub.feature_list.tag(feat.clone()).unwrap().0) == "dlig").flat_map(|feat|gsub.feature_list.table(feat).unwrap().unwrap().lookups).collect();
	println!("{:?}", lookups);
	println!("ffi: {}", font.cmap.lookup('\u{fb03}').unwrap().0);
	println!("ff: {}", font.cmap.lookup('\u{fb00}').unwrap().0);
	println!("fl: {}", font.cmap.lookup('\u{fb02}').unwrap().0);
	//println!("{:#?}", font.cmap);
	let gdef = collection.opentype.unwrap().gdef;
	println!("before: {:?}", text);
	for lookup in lookups {
		let (lut, config) = gsub.lookups.get_lookup(lookup).unwrap().unwrap();
		println!("{:#?}", lut);
		struct Feedback; impl SubstitutionFeedback for Feedback {fn merge(&mut self, target: usize, source: usize) {println!("merging {} into {}", source, target)}}
		text = run(gsub.lookups, lookup, text.into_iter(), gdef.class_def, &mut Feedback).unwrap(); 
		println!("after {:?}: {:?}", lookup, text);
	}
}
